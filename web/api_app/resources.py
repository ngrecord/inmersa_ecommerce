#!/usr/bin/python
#  -*- coding: utf-8 -*-

from tastypie.resources import ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from plate_app.models import Plate
from django.contrib.auth.models import User

class PlateResource(ModelResource):
    class Meta:
        queryset = Plate.objects.all()
        resource_name = 'plate'

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['password', 'is_superuser']
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()

    def obj_create(self, bundle, **kwargs):
        return super(UserResource, self).obj_create(bundle, user=bundle.request.user)

    def authorized_read_list(self, object_list, bundle):
        return object_list.filter(username=bundle.request.user)