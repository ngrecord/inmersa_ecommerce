from django.contrib import admin
from .models import Plate, Ingredient, Place

class PlateAdmin(admin.ModelAdmin):
    pass
admin.site.register(Plate, PlateAdmin)

class IngredientAdmin(admin.ModelAdmin):
    pass
admin.site.register(Ingredient, IngredientAdmin)

class PlaceAdmin(admin.ModelAdmin):
    pass
admin.site.register(Place, PlaceAdmin)