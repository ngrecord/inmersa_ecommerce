from django.db import models



class Place(models.Model):
    name = models.CharField('Lugar', max_length=50, help_text='Sector o Zona')

    def __str__(self):
        return self.name

class Ingredient(models.Model):
    name = models.CharField('Ingrediente', max_length=50, help_text='Nombre de Ingrediente')

    def __str__(self):
        return self.name

class Plate(models.Model):
    title = models.CharField('Titulo',max_length=50, help_text='Titulo del Plato')
    info = models.TextField('Informacion', help_text='Informacion del Plato')
    price = models.DecimalField('Precio', max_digits=15, decimal_places=3, help_text='Precio o Valor')
    image = models.ImageField('Imagen', upload_to='plates/', null=True, help_text='Imagen o Fotografia')
    place = models.ForeignKey(Place)
    ingredient = models.ManyToManyField(Ingredient)

    def __str__(self):
        return self.title


