var galleryModule = angular.module('galleryModule', ['ngRoute', 'ngResourceTastypie']);

galleryModule.config(['$routeProvider', '$tastypieProvider',
    function ($routeProvider, $tastypieProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'static/scripts/app/html/gallery.html',
            controller: 'homeCtrl'
        }).otherwise({
            redirectTo: '/home'
        });

        $tastypieProvider
            .add('provider', {
                url: 'http://162.243.193.110/api/v1/'
            });

        $tastypieProvider.setDefault('provider');
    }]);


galleryModule.controller('homeCtrl', ['$scope', '$http', '$tastypieResource', '$tastypie',
    function ($scope, $http, $tastypieResource, $tastypie) {
        $scope.Plates = new $tastypieResource('plate', {limit:9});
        $scope.Plates.objects.$find();
    }]);

galleryModule.controller('menuCtrl', ['$scope', '$http', '$tastypieResource', '$tastypie',
    function ($scope, $http, $tastypieResource, $tastypie) {
        $scope.Users = new $tastypieResource('user', {limit:1});
        $scope.Users.objects.$find();
    }]);

