# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='Ingrediente', help_text='Nombre de Ingrediente', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='Lugar', help_text='Sector o Zona', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Plate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('title', models.CharField(verbose_name='Titulo', help_text='Titulo del Plato', max_length=50)),
                ('info', models.TextField(verbose_name='Informacion', help_text='Informacion del Plato')),
                ('price', models.DecimalField(verbose_name='Precio', help_text='Precio o Valor', decimal_places=3, max_digits=15)),
                ('image', models.ImageField(verbose_name='Imagen', help_text='Imagen o Fotografia', null=True, upload_to='plates/')),
                ('ingredient', models.ManyToManyField(to='plate_app.Ingredient')),
                ('place', models.ForeignKey(to='plate_app.Place')),
            ],
        ),
    ]
