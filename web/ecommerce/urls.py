#!/usr/bin/python
#  -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView

from tastypie.api import Api
from api_app.resources import PlateResource, UserResource

v1_api = Api(api_name='v1')
v1_api.register(PlateResource())
v1_api.register(UserResource())

urlpatterns = [
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('user_app.urls')),
    url(r'^$', login_required(TemplateView.as_view(template_name="index.html"))),
    url(r'^api/', include(v1_api.urls)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]